import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Grid, Segment} from 'semantic-ui-react';
import logo from './logo.svg';
import './App.css';
import * as ActionCreators from './redux/actions';
import Items from './components/Items';
import MiniCart from "./components/MiniCart";

class App extends Component {

  componentDidMount() {
    this.props.actions.getProducts();
    this.props.actions.getCart();
  }

  render() {
    return (<div>
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo"/>
          <h1 className="App-title">React Cart Demo - JG</h1>
        </header>
        <div className="App">
          <Grid>
            <Grid.Row>
              <Grid.Column floated='left' width={6}>
                <Segment raised={true}>
                  <Items products={this.props.products}
                         addProduct={this.props.actions.addProduct}
                  />
                </Segment>
              </Grid.Column>
              <Grid.Column floated='right' width={4}>
                <MiniCart cart={this.props.cart}
                          products={this.props.products}
                          deleteProduct={this.props.actions.deleteProduct}
                          addProduct={this.props.actions.addProduct}
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state, props) {
  if (state === undefined) {
    return {cart: {}, products: {}};
  }
  const cart = state.cart;
  const products = state.products;
  return {...props, cart, products}
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(ActionCreators, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
