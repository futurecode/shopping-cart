import React from 'react';
import {Table, Icon, Button, Modal} from 'semantic-ui-react';
import Cart from "./Cart";

class MiniCart extends React.Component {

  // avoiding floats prices are cents internally
  formatEuro(val) {
    return val.toString().replace(/^(.*)(..)$/, "$1,$2 €");
  }

  render() {
    if (!this.props.cart)
      return [];

    //@todo move to model/api and let the table below fill by data object
    let netTotal = 0;
    let taxTotal = 0;
    let taxBreakDown = [];
    Object.keys(this.props.cart).forEach(productId => {
      const prod = this.props.products[productId];
      const amount = this.props.cart[prod.id];
      const tax = (Math.round(prod.price / 100 * prod.tax) * amount)
      netTotal += prod.price * amount;
      taxBreakDown[prod.tax] = tax + (taxBreakDown[prod.tax] || 0);
      taxTotal += tax;
    });
    let grossTotal = netTotal + taxTotal;


    // "noop" property hides the open checkout button
    // this is because MiniCart is also used in the Cart Component to show the SUM.
    const OpenCartButton = this.props.noop === undefined || !this.props.noop
      ? <Table.Row>
        <Table.HeaderCell colspan={2}>
          <Modal trigger={<Button positive><Icon name="check"/>Open Shopping Cart</Button>}>
            <Modal.Header>Please proceed to checkout</Modal.Header>
            <Modal.Content>
              <Cart products={this.props.products}
                    cart={this.props.cart}
                    deleteProduct={this.props.deleteProduct}
                    addProduct={this.props.addProduct}/>
            </Modal.Content>
          </Modal>
        </Table.HeaderCell>
      </Table.Row>
      : [];

    const TaxDetailRows = taxBreakDown.map((sum, taxValue) =>
      <Table.Row>
        <Table.Cell><Icon name="arrow right"/> {taxValue}% tax</Table.Cell>
        <Table.Cell>{this.formatEuro(sum)}</Table.Cell>
      </Table.Row>);

    return (<Table size="mini">
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell><Icon name="cart"/>Cart Details</Table.HeaderCell>
          <Table.HeaderCell>Sum</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        <Table.Row>
          <Table.Cell>Net Total</Table.Cell>
          <Table.Cell>{this.formatEuro(netTotal)}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>Tax Total</Table.Cell>
          <Table.Cell>{this.formatEuro(taxTotal)}</Table.Cell>
        </Table.Row>
        {TaxDetailRows}
      </Table.Body>
      <Table.Footer>
        <Table.Row>
          <Table.HeaderCell><strong>Gross Total</strong></Table.HeaderCell>
          <Table.HeaderCell><strong>{this.formatEuro(grossTotal)}</strong></Table.HeaderCell>
        </Table.Row>
        {OpenCartButton}
      </Table.Footer>
    </Table>);
  }
}


export default MiniCart;