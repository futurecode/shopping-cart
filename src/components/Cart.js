import React from 'react';
import {Table, Button, Icon, Popup} from 'semantic-ui-react';
import CartRow from "./CartRow";
import MiniCart from "./MiniCart";

class Cart extends React.Component {

  render() {
    const cartItems = Object.keys(this.props.cart).map(
      productId => <CartRow deleteProduct={this.props.deleteProduct}
                            addProduct={this.props.addProduct}
                            amount={this.props.cart[productId]}
                            product={this.props.products[productId]}
                            key={productId}/>
    );
    return (
      <div>
        <Table celled compact definition>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell/>
              <Table.HeaderCell>Amount</Table.HeaderCell>
              <Table.HeaderCell>Product Title</Table.HeaderCell>
              <Table.HeaderCell>Price</Table.HeaderCell>
              <Table.HeaderCell>Sum (Gross)</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {cartItems}
            <Table.Row>
              <Table.Cell/>
              <Table.Cell colspan={4}></Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell/>
              <Table.Cell colspan={2}></Table.Cell>
              <Table.Cell colspan={2}>
                <MiniCart noop={true}
                          products={this.props.products}
                          cart={this.props.cart}/>
              </Table.Cell>
            </Table.Row>
          </Table.Body>

          <Table.Footer>
            <Table.Row>
              <Table.HeaderCell/>
              <Table.HeaderCell colSpan='4'>
                <Button floated='right' icon labelPosition='left' primary positive>
                  <Icon name='shop'/> Place Order
                </Button>
                <Popup
                  trigger={<Button size='small'><Icon name="save"/> Save Cart for later</Button>}
                  content='You need to be logged in first'
                  inverted={true}
                />
              </Table.HeaderCell>
            </Table.Row>
          </Table.Footer>
        </Table>
      </div>);
  }
}


export default Cart;
