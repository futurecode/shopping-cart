import React from 'react';
import {Table, ButtonGroup, Button, Input} from 'semantic-ui-react';

class CartRow extends React.Component {

  // avoiding floats
  formatEuro(val) {
    return val.toString().replace(/^(.+)(..)$/, "$1,$2 €");
  }

  render() {
    if (this.props.amount < 1) {
      return [];
    }
    const tax = Math.round(this.props.product.price / 100 * this.props.product.tax);
    const priceGross = this.props.product.price + tax;
    const sum = priceGross * this.props.amount;
    return (<Table.Row>
        <Table.Cell collapsing>
          <ButtonGroup size="mini">
            <Button icon="minus" compact={true} negative onClick={() => this.props.deleteProduct(this.props.product)} />
            <Button icon="plus" compact={true} primary onClick={() => this.props.addProduct(this.props.product)} />
          </ButtonGroup>
        </Table.Cell>
        <Table.Cell collapsing><Input
          type="text" size="mini" disabled={true} input={{size:2, value: this.props.amount}} />
        </Table.Cell>
        <Table.Cell>{this.props.product.name}</Table.Cell>
        <Table.Cell>
          <strong>{this.formatEuro(priceGross)}</strong>
          <br/>
          <small><em>{this.formatEuro(this.props.product.price)} net plus {this.props.product.tax}% tax</em></small>
        </Table.Cell>
        <Table.Cell><strong>{this.formatEuro(sum)}</strong></Table.Cell>
      </Table.Row>
    );
  }
}

export default CartRow;