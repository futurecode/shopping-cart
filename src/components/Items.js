import React from 'react';
import {Item, Button, Icon, Label} from 'semantic-ui-react';

class Items extends React.Component {

  render() {

    const ItemList = Object.keys(this.props.products).map(productId => (
      <Item key={productId}>
        <Item.Image src="/product.png"/>
        <Item.Content>
          <Item.Header as='a'>{this.props.products[productId].name}</Item.Header>
          <Item.Meta>
            <span className='cinema'>{this.props.products[productId].description}</span>
          </Item.Meta>
          <Item.Description>{this.props.products[productId].price
            .toString().replace(/^(.+)(..)$/, "$1,$2 €")} net</Item.Description>
          <Item.Extra>
            <Button color="orange" onClick={e => this.props.addProduct(this.props.products[productId])}>
              <Icon name='add to cart'/>
              Add to cart
            </Button>
            <Label>Limited offer</Label>
          </Item.Extra>
        </Item.Content>
      </Item>)) || [];

    return (
      <Item.Group divided>
        {ItemList}
      </Item.Group>
    );
  }
}

export default Items;
