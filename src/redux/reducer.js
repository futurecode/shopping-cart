import {
  ADD_PRODUCT,
  DELETE_PRODUCT,
  GET_PRODUCTS,
  GET_CART
} from './actions';

function cartReducer(state, action) {
  let cart = {};
  if (action === undefined) {
    return state;
  }
  switch (action.type) {

    case ADD_PRODUCT:
      const value = state.cart[action.product.id] ? state.cart[action.product.id] : 0;
      cart = Object.assign({}, state.cart);
      cart[action.product.id] = 1 + value;
      return {...state, cart};

    case DELETE_PRODUCT:
      cart = Object.assign({}, state.cart);
      cart[action.product.id] -= 1;
      return {...state, cart};

    case GET_CART:
       cart = Object.assign({}, state.cart);
      return {...state, cart};

    case GET_PRODUCTS:
      const products = {
        1: {id:1, name: 'Visitor NFC Card', price:1007, tax:19, description: 'Grant access to individuals via programmable NFC Card.'},
        2: {id:2, name: 'NFC Card Writer', price:10000, tax:19, description: 'This device let\'s you write on our NFC Cards.'},
        3: {id:3, name: 'Space Ball Pen', price:1493, tax:7, description: 'Write upside down as well as submerged in water.'}
      };
      return {...state, products};

    default:
      return state;
  }

}

export default cartReducer;