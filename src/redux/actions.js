export const ADD_PRODUCT = 'ADD_PRODUCT';
export const DELETE_PRODUCT = 'DELETE_PRODUCT';
export const GET_PRODUCTS = 'GET_PRODUCTS';
export const GET_CART = 'GET_CART';

export function addProduct(product) {
  return { type: ADD_PRODUCT, product }
}
export function deleteProduct(product) {
  return { type: DELETE_PRODUCT, product }
}
export function getProducts() {
  return {type: GET_PRODUCTS}
}
export function getCart() {
  return {type: GET_CART}
}
